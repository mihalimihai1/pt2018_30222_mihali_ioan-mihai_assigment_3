package model;
public class produs {
	public produs(int idprodus, String denumire, String cantitate, String pret) {
		this.idprodus = idprodus;
		this.denumire = denumire;
		this.cantitate = cantitate;
		this.pret = pret;
	}
	int idprodus;
	String denumire;
	String pret;
	String cantitate;
	public int getIdprodus() {
		return idprodus;
	}
	public void setIdprodus(int idprodus) {
		this.idprodus = idprodus;
	}
	public String getNume() {
		return denumire;
	}
	public void setNume(String denumire) {
		this.denumire = denumire;
	}
	public String getPret() {
		return pret;
	}
	public void setPret(String pret) {
		this.pret = pret;
	}
	public String getCantitate() {
		return cantitate;
	}
	public void setCantitate(String cantitate) {
		this.cantitate = cantitate;
	}
	@Override
	public String toString() {
		return "produs [idprodus=" + idprodus + ", nume=" + denumire + ", pret=" + pret + ", cantitate=" + cantitate + "]";
	}
	
	
}
