package presentation;
import dao.*;
import model.comanda;
import model.produs;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;


public class GUIprodus {
	private JButton b1, b2, b3;
	public JPanel panou, panou2;///
	private JLabel l1, l2, l3, l4;
	private JTextField t1, t2, t3, t4;
	public JFrame frame = new JFrame("Produsele");
	private ArrayList<produs> client = new ArrayList<>();
	private JTable table = new JTable();
	private DefaultTableModel mod = (DefaultTableModel) table.getModel();
	produsDAO daoP = new produsDAO();

	comanda c = null;

	public GUIprodus() {

		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLayout(new GridLayout(1, 2));
		frame.setSize(600, 300);
		frame.setBackground(Color.YELLOW);
		frame.setForeground(Color.YELLOW);

		panou = new JPanel();
		panou.setLayout(null);
		panou.setBackground(Color.PINK);
		panou2 = new JPanel();

		b1 = new JButton("Add");
		b2 = new JButton("Delete");
		b3 = new JButton("Update");

		l1 = new JLabel("IDprodus:");
		l2 = new JLabel("Denumire:");
		l3 = new JLabel("Cantitate:");
		l4 = new JLabel("Pret:");

		frame.setBounds(150, 100, 650, 300);

		l1.setBounds(10, 10, 100, 18); // x y lungime latime
		panou.add(l1);
		l2.setBounds(10, 30, 100, 18);
		panou.add(l2);
		l3.setBounds(10, 50, 100, 18);
		panou.add(l3);

		t1 = new JTextField("");
		t1.setBounds(90, 10, 150, 18);
		panou.add(t1);
		t2 = new JTextField("");
		t2.setBounds(90, 30, 150, 18);
		panou.add(t2);
		t3 = new JTextField("");
		t3.setBounds(90, 50, 150, 18);
		panou.add(t3);

		b1.setBounds(10, 90, 100, 40);
		panou.add(b1);
		/////////////////
		l4.setBounds(10, 70, 150, 18);
		panou.add(l4);
		t4 = new JTextField("");
		t4.setBounds(90, 70, 150, 18);
		panou.add(t4);
		b2.setBounds(115, 90, 100, 40);
		panou.add(b2);
		b3.setBounds(10, 140, 100, 40);
		panou.add(b3);

		frame.add(panou);
		frame.add(panou2);
		////////////////////
		panou2.add(daoP.populatefirst());

		b1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				daoP.inserare(new produs(Integer.parseInt(t1.getText()), t2.getText(), t3.getText(), t4.getText()));
				repopulateTable(daoP.listaClienti);
			}
		});

		b2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				daoP.delete(new produs(Integer.parseInt(t1.getText()), t2.getText(), t3.getText(), t4.getText()));
				repopulateTable(daoP.listaClienti);
			}
		});

		b3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				daoP.update(new produs(Integer.parseInt(t1.getText()), t2.getText(), t3.getText(), t4.getText()));
				repopulateTable(daoP.listaClienti);
			}
		});

		frame.setVisible(true);
	}

	public void repopulateTable(ArrayList<produs> list) {
		// panou2.removeAll(); // ii dam remove la tabelul vechi
		frame.remove(panou2);
		frame.add(table);
		// punem tabelul nou
		table.setModel(mod); // punem modelul in tabel
		mod.setRowCount(0); // curatam tabelul
		mod.setColumnCount(0);

		Object[] obiecte = new Object[4];

		mod.addColumn("id");
		mod.addColumn("nume");
		mod.addColumn("email");
		mod.addColumn("telefon");

		mod.setRowCount(0);
		for (int i = 0; i < list.size(); i++) {
			obiecte[0] = list.get(i).getIdprodus();
			obiecte[1] = list.get(i).getNume();
			obiecte[2] = list.get(i).getCantitate();
			obiecte[3] = list.get(i).getPret();
			mod.insertRow(i, obiecte); // adaugam cate un rand
			System.out.println(obiecte[0]);
		}

	}
}
