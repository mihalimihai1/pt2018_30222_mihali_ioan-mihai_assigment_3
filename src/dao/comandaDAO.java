package dao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import javax.swing.JTable;

import connection.Conexiune;
import model.comanda;
import model.produs;

public class comandaDAO {
	private final static String findStatementString = "SELECT * FROM alcool2.comanda;";
	private final static String insertStatementString = "INSERT INTO `alcool2`.`comanda` (`idorder`,`numeProdus`, `idClient`, `cantitate`, `pret`,`total`) VALUES (?, ?, ?, ?, ?,?);";
	public ArrayList<produs> produse = new ArrayList<produs>();
	private int count() {
		Connection ConBd = Conexiune.getConnection();
		ResultSet rs = null;
		int val = 0;
		Statement statement = null;

		try {
			statement = ConBd.createStatement();
			rs = statement.executeQuery("SELECT count(*) FROM alcool2.comanda");
			while (rs.next()) {
				val = rs.getInt(1);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return val;

	}

	public JTable populatefirst() {
		Object[][] obiecte = new Object[count()][6];
		int nrinregistrari = 0;
		int all = 0;
		String[] coloane = { "idorder", "numeprodus", "idclient", "cantitate", "pret", "total" };
		Connection ConBd = Conexiune.getConnection();
		ResultSet reset = null;
		PreparedStatement findStatement = null;
		try {
			findStatement = ConBd.prepareStatement(findStatementString);
			reset = findStatement.executeQuery();

			while (reset.next()) {
				System.out.println(reset.getInt("idorder"));
				obiecte[nrinregistrari][0] = reset.getInt("idorder");
				obiecte[nrinregistrari][1] = reset.getString("numeprodus");
				obiecte[nrinregistrari][2] = reset.getInt("idclient");
				obiecte[nrinregistrari][3] = reset.getInt("cantitate");
				obiecte[nrinregistrari][4] = reset.getInt("pret");
				obiecte[nrinregistrari][5] = reset.getDouble("total");
				nrinregistrari++;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			Conexiune.close(ConBd);
			Conexiune.close(reset);
			Conexiune.close(findStatement);
		}

		return new JTable(obiecte, coloane);
	}

	public ArrayList<comanda> listaClienti = new ArrayList<comanda>();

	public void inserare(comanda client) {
		Connection ConBd = Conexiune.getConnection();
		PreparedStatement insertStatement = null;
		PreparedStatement findStatement = null;
		ResultSet rs = null;
		try {
			insertStatement = ConBd.prepareStatement(insertStatementString);
			findStatement = ConBd.prepareStatement(findStatementString);
			insertStatement.setInt(1, client.getIdorder());
			insertStatement.setString(2, client.getNumeProdus());
			insertStatement.setInt(3, client.getIdClient());
			insertStatement.setInt(4, client.getCantitate());
			insertStatement.setInt(5, client.getPret());
			insertStatement.setDouble(6, (client.getPret()) * (client.getCantitate()));
			insertStatement.executeUpdate();
			rs = findStatement.executeQuery();
			listaClienti.clear();
			while (rs.next())
				listaClienti.add(new comanda(rs.getInt("idorder"), rs.getString("numeprodus"), rs.getInt("idclient"),
						rs.getInt("cantitate"), rs.getInt("pret"), rs.getDouble("total")));
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			Conexiune.close(ConBd);
			Conexiune.close(insertStatement);
			Conexiune.close(findStatement);

		}
	}

	public void changeStock(String numeProdus, int cantitateComanda) {
		Connection ConBd = Conexiune.getConnection();
		PreparedStatement findStatement = null;
		PreparedStatement updateStatement = null;
		ResultSet rs = null;
	

		try {
			findStatement = ConBd.prepareStatement("SELECT * FROM alcool2.produs;");
			updateStatement = ConBd.prepareStatement("UPDATE alcool2.produs SET cantitate=? WHERE denumire=?;");
			rs = findStatement.executeQuery();
			listaClienti.clear();
			while (rs.next())
				produse.add(new produs(rs.getInt("idprodus"), rs.getString("denumire"), rs.getString("cantitate"),
						rs.getString("pret")));

			for (produs prd : produse) {
				System.out.println(prd.getNume());
				if (prd.getNume().equals(numeProdus)) {
					updateStatement.setString(1,
							Integer.toString((Integer.parseInt(prd.getCantitate()) - cantitateComanda)));
	
					updateStatement.setString(2, numeProdus);
					updateStatement.executeUpdate();
					prd.setCantitate(Integer.toString((Integer.parseInt(prd.getCantitate()) - cantitateComanda)));
				}
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			Conexiune.close(ConBd);
			Conexiune.close(rs);
			Conexiune.close(findStatement);
		}

	}
}
